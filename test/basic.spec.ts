import { marbles } from 'rxjs-marbles/mocha'
import { map, mapTo, take } from 'rxjs/operators'
import { interval, merge, Observable } from 'rxjs'


describe( 'basic', () => {
    it( 'should compare marble observables', marbles( m => {
        const inputs = {
            a: 1,
            b: 2,
            c: 3
        }

        const outputs = {
            x: 2,
            y: 3,
            z: 4
        }

        const source = m.hot( '--^-a-b-c-|', inputs )
        const subs =                     '^-------!'
        const expected = m.cold( '--x-y-z-|', outputs )
        const destination = source.pipe( map( val => val + 1 ) )

        m.expect( destination ).toBeObservable( expected )
        m.expect( source ).toHaveSubscriptions( subs )
    } ) )

    it('should compare a real observable to marbles', marbles(m => {

        const first: Observable<number> = interval(20) //every 20ms
        const second: Observable<number> = interval(15) //every 15ms
        const third: Observable<number> = interval(10) //every 10ms
        const fourth: Observable<number> = interval(5) //every 5ms

        const source = merge(
            first.pipe(take(1), mapTo('first')),
            second.pipe(take(1), mapTo('second')),
            third.pipe(take(2), mapTo('third')),
            fourth.pipe(take(4), mapTo('fourth'))
        )

        const expected = m.cold('-----d----(cd)-(bd)-(acd|)', {
            a: 'first',
            b: 'second',
            c: 'third',
            d: 'fourth'
        })
        m.expect(source).toBeObservable(expected)

        source.subscribe(console.log)
    }))
} )
