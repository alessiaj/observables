import { forkJoin, interval, Observable, of } from 'rxjs'
import { delay, take } from 'rxjs/operators'

// When all observable completes
// give the last value from each as an array
const example: Observable<[string, string, number]> = forkJoin([
    of('Hello'),
    of('World').pipe(delay(1000)),
    interval(2000).pipe(take(2))
])

example.subscribe(console.log)
