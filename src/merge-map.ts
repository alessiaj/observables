import { Observable, of } from 'rxjs'
import { mergeMap } from 'rxjs/operators'

const source: Observable<string> = of( 'Hello' );

const example1: Observable<string> = source.pipe(
    mergeMap(
        val => of( `${ val } World` )
    )
);

example1.subscribe( console.log );
