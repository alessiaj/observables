import { EMPTY } from 'rxjs'

EMPTY.subscribe({
  next: () => console.error('Next'),
  complete: () => console.log('Complete')
});
