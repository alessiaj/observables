import { interval, Observable, zip } from 'rxjs'

const source1: Observable<number> = interval(1000); // emits every second
const source2: Observable<number> = interval(2000);
const source3: Observable<number> = interval(3000);

let counter = 0;
setInterval(() => console.log(++counter), 1000);

// wait until all observables emit a value and emit all as an array
zip(source1, source2, source3).subscribe(console.log);
