import { from, of } from 'rxjs'

from([1, 2, 3, 4, 5]).subscribe(
    val => console.log(val),
    err => console.error(err),
    () => console.log('Complete')
);

from(new Promise(resolve => resolve('Hello world from Promise'))).subscribe(console.log)

of(1, 2, 3).subscribe(console.log, undefined, () => console.log('complete'))
